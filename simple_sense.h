/*
 * Derived from examples/aws_iot/aws_iot.c - added sensors and actuators support service
 */
#include "simple_sense_config.c"
#include "espressif/esp_common.h"
#include "esp/uart.h"

#include <string.h>

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <private_ssid_config.h>

#include <espressif/esp_sta.h>
#include <espressif/esp_wifi.h>

#include <paho_mqtt_c/MQTTESP8266.h>
#include <paho_mqtt_c/MQTTClient.h>

// this must be ahead of any mbedtls header files so the local mbedtls/config.h can be properly referenced
#include "ssl_connection.h"

typedef enum{
    simple_sense_soft_pause,
    simple_sense_deep_pause,
    simple_sense_run
}SS_Stat;

void sync(void *pvParameters);
void simple_sense(void *pvParameters);
void wifi_task(void *pvParameters);
void simple_sense_controller(int command);
void sensor_init(const char * name, uint8_t port, int (*callback)(uint8_t));
void actuator_init(char * name, void (*callback)(char*));
