/*
 * This file define parameters for your Simple Sense application
 * 
 */

/* The name of thing attached to your project 
 * If you are not using AWS IOT THINGS resource keep the macro as ""
 */
#define AWS_IOT_THING_NAME "Ganso"

/* 
 * NUMBER_OF_SENSORS defines de the max number of sensors supported the by application
 * Minimum = 1
 */
#define NUMBER_OF_SENSORS 4

/* 
 * NUMBER_OF_ACTUATORS defines de the max number of actuators supported the by application
 * Minimum = 1
 */
#define NUMBER_OF_ACTUATORS 2

/*
 * REFRESH_RATE is the delay between sensor's update messages in MS
 */
#define REFRESH_RATE 10000

/*
 * PUBLISH_LENGHT define the max lenght of your json message.
 * It is also recommended to keep it as low as possible.
 */
#define PUBLISH_LENGHT 100

/*
 * MQTT_PUB_TOPIC is the topic that receives the sensor's update
 */
#define MQTT_PUB_TOPIC "esp8266/status"

/*
 * MQTT_SUB_TOPIC is the topic your actuators will receive commands from.
 */
#define MQTT_SUB_TOPIC "esp8266/control"

/* 
 * MQTT_PORT is the port the application use to connect to your MQTT server
 */
#define MQTT_PORT 8883

/*
 * MQTT_CLIENT_ID defines the id for your mqtt connection
 */
#define MQTT_CLIENT_ID "PSE-BOT-"

/*
 * MQTT_BUFFER_SIZE is the size of the buffer for your mqtt connection
 * if you don't know what are doing keep it at 100
 */
#define MQTT_BUFFER_SIZE 100

/*
 * MQTT_READBUFFER_SIZE is the size of the readbuffer for your mqtt connection
 * if you don't know what are doing keep it at 100
 */
#define MQTT_READBUFFER_SIZE 100