
#include "simple_sense.h"

#define SENSOR_PIN 5

typedef enum{
    disabled,
    enabled,
    silent
}Stat;

struct{
    Stat status;
    int id;
}Ganso;

void Ganso_controller(const char* key_value){
    Ganso.status = atoi(key_value);

    if(Ganso.status > 0)
        simple_sense_controller(simple_sense_run);
    else if(Ganso.status == 0)
        simple_sense_controller(simple_sense_soft_pause);

    char buffer[100];
    snprintf(buffer, 100,"{\n\"state\" : {\n\"reported\" : { \"stat\": %d}\n}\n}",Ganso.status);
    simple_publish(buffer, "$aws/things/Ganso/shadow/update");
    printf("Ganso Stat %d\n",Ganso.status);
}

void sensor_intr_handler(uint8_t gpio_num){
    printf("Interrupted\n");
    security_protocol();
}

void security_protocol(){
    //buzzer alert
    if(Ganso.status != disabled){
        }

}

void ganso_init(){
    Ganso.status = disabled;
    Ganso.id = 51;

    gpio_enable(SENSOR_PIN, GPIO_INPUT);

    sensor_init("PIR_SENSOR:", 5, NULL);
    
    actuator_init("stat",Ganso_controller);

    gpio_set_interrupt(SENSOR_PIN, GPIO_INTTYPE_EDGE_ANY, sensor_intr_handler);

    simple_sense_controller(simple_sense_run);

    printf("Ganso initialization done.\n");
}

void user_init(void) {
    uart_set_baud(0, 115200);

    ganso_init();
    xTaskCreate(&wifi_task, "wifi_task", 256, NULL, 2, NULL);
    xTaskCreate(&sync, "sync", 256, NULL, 2, NULL);
    xTaskCreate(&simple_sense, "simple_sense", 2048, NULL, 2, NULL);

}
